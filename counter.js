class Counter {
    element;
    _timer = 30;

    constructor(startTime, getElementById){
        this._timer = startTime;
        this.element = document.getElementById('counter');
    }

    get timer(){
        return this._timer;
    }

    set timer(value){
        this._timer = value;
        this.element.textContent = Math.trunc(value);
    }

    update () {
        if(this._timer <= 0) 
            gameManager.stop();     // is working
            return;
        }
}