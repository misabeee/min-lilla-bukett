class GameManager {
	// Properties
	gameObjects = [];
	canvas = null;
	previousTimeStamp;
	currentDeltaTime;
	currentTimeStamp;
	currentTimer;
	counter;

    constructor() {
        window.gameManager = this;
        window.gravityHelper = new GravityHelper();
        console.log("gameManager created");
		this.isActive = false;
    }
	
	// Functions
	
	gameLoop() {
		let currentTimeStamp = performance.now();
        gameManager.currentDeltaTime = currentTimeStamp - gameManager.previousTimeStamp;
        gameManager.previousTimeStamp = currentTimeStamp;
        if(!isNaN(gameManager.currentDeltaTime)){
            gameManager.counter.timer -= gameManager.currentDeltaTime * .001;
        }

		canvas.drawLayer.clearRect(0, 0, canvas.canvasHTMLElement.width, canvas.canvasHTMLElement.height);
		for (let gameLoopState = 0; gameLoopState < 5; gameLoopState++) {
			// gameLoopState 0 -> store positions of and update all objects 
			// gameLoopState 1 -> check if the updated objects are colliding
			// gameLoopState 2 -> apply Gravity Forces to all objects (where useGravity == true)
			// gameLoopState 3 -> check if objects (after gravity) are hitting something

			gameManager.gameObjects.forEach((gameObject) => {
				if (gameObject.isActive) {
					if (gameLoopState == 0) {
						gameObject.storePosition();
						gameObject.update();
					}
					if (gameLoopState == 1) {
						gameObject.currentGravityCollisionObject = null;
						gameManager.checkObjectsForCollisions(gameObject);
					}
					if (gameLoopState == 2 && gameObject.useGravity) {
						gravityHelper.applyGravityForces(gameObject, false);
					}
					if (gameLoopState == 3) {
						gameManager.checkObjectsForGravityCollisions(gameObject);
					}
					if (gameLoopState == 4) {
						if (gameObject.useGravity) {
							if (gameObject.currentGravityCollisionObject != null) {
								gravityHelper.applyGravityForces(gameObject, true);
								gravityHelper.applyGameObjectToHitPlatform(gameObject);
							}
							else {
								gameObject.isFalling = true;
							}
						}
						
						gameObject.draw();
					}
				}
				gameManager.counter.update();
			});
		}
		requestAnimationFrame(gameManager.gameLoop);
	}

	checkObjectsForCollisions(object1) {
		for (let i = object1.gameObjectIndex + 1; i < gameManager.gameObjects.length; i++) {
			let object2 = gameManager.gameObjects[i];
			if(object2.isActive) {
				// normal collision after update
				let collisionDetected = this.detectCollision(object1, object2);
				if (collisionDetected) {
					object1.onCollision(object2);
					object2.onCollision(object1);
				}	
			}
		}
	}
	
	checkObjectsForGravityCollisions(object1) {	
		for (let i = object1.gameObjectIndex + 1; i < gameManager.gameObjects.length; i++) {
			let object2 = gameManager.gameObjects[i];
			if(object2.isActive && object2.isRigid && object1.useGravity) {
				gravityHelper.checkForGravityCollision(object1, object2);
			}
			if(object2.isActive && object1.isRigid && object2.useGravity) {
				gravityHelper.checkForGravityCollision(object2, object1);
			}
		}
	}


	detectCollision(object1, object2) {
		//overlap on x axis
		if(object1.boundaries.getLeftBoundary() <= object2.boundaries.getRightBoundary() &&
			object1.boundaries.getRightBoundary() >= object2.boundaries.getLeftBoundary()) {
					//overlap on y axis
			if(object1.boundaries.getTopBoundary() <= object2.boundaries.getBottomBoundary() &&
				object1.boundaries.getBottomBoundary() >= object2.boundaries.getTopBoundary()) {
					return true;
			}
		}
	}	

	addGameObject(object) {
		this.gameObjects.push(object);
		object.gameObjectIndex = this.gameObjects.length - 1;
	}

	setCanvas(canvas) {
		this.canvas = canvas;
	}	

	

	// Startbutton starts game
	start() {
		this.counter = new Counter(30, '#counter');
		this.toggleScreen('start-screen', false);
		this.toggleScreen('gameContainer', true);
		this.toggleScreen('gameover-screen', false);
		this.toggleScreen('youwon-screen', false);
		requestAnimationFrame(gameManager.gameLoop);
	}

	// Game-over-screen
	stop() {
		this.toggleScreen('start-screen', false);
		this.toggleScreen('gameContainer', false);
		this.toggleScreen('gameover-screen', true);
		this.toggleScreen('youwon-screen', false);
	}

	// You won screen
	won() {
		this.toggleScreen('start-screen', false);
		this.toggleScreen('gameContainer', false);
		this.toggleScreen('gameover-screen', false);
		this.toggleScreen('youwon-screen', true);
	}

	// For the the different screens
	toggleScreen(id, toggle) {
		let element = document.getElementById(id);
		let display = ( toggle ) ? 'block' : 'none';
		element.style.display = display;
	}

	// Reloads page, so game starts from new
	restartGame() {
		location.reload();
	}
}
