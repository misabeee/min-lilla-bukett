class Score extends GameObject {
    score = 0;
    image = new Image();
    isLoaded = false;
    element;

    constructor(ScoreId) {
        super("score", 60, 20, 80, 25);
        this.image.src = "./images/score-sign.png";
        this.image.addEventListener("load", () => {
            this.isLoaded = true;
        });
        this.element = document.querySelector("#score");
        this.element.textContent = "0/8";
    }
    
    increaseScore() {
        this.score++;
        this.element.textContent = this.score + "/8";
    }

}