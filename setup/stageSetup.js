let gameManager = new GameManager();

new GameManager();

// Floor Map
let floorMap = [    
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
];

let canvas = new Canvas("canvas");

// Player Figure
let alice = new PlayerFigure("player", 25, 270, 34, 80, "images/alice-spritesheet.png");
alice.addAnimationInformation("walk_left", 5, 9);
alice.addAnimationInformation("walk_right", 0, 4);

alice.addAnimationInformation("idle_left", 5, 5);
alice.addAnimationInformation("idle_right", 0, 0);

alice.addAnimationInformation("jump_left", 15, 19);
alice.addAnimationInformation("jump_right", 10, 14);

// Enemy
let enemy = new Enemy("enemy", 634, 450, 37, 33, "images/dog-Sheet.png");
enemy.addAnimationInformation("enemy_walk_left", 0, 3);
enemy.addAnimationInformation("enemy_walk_right", 4, 7);


function setupFloor() {
    for (let y = 0; y < floorMap.length; y++) {
        for(let x = 0; x < floorMap[y].length; x++) {
            if (floorMap[y][x] == 1) {
               new Floor("floor", x * 64, y * 109, 64, 64, "images/gras-bottom.png");
            }
        }
    }
} 

setupFloor();

// Side-scrolling effect
let moveRight = new MoveLevel("moveRight", 510, 0, 20, 500, "gameContainer");
let moveLeft = new MoveLevel("moveLeft", 210, 0, 20, 500, "gameContainer");

// Upgrades
new Upgrade("apple", 420, 56, 22, 30, "images/apple.png");
new Upgrade("apple", 929, 177, 22, 30, "images/apple.png");

// Downgrades
new Downgrade("badFlower", 1008, 354, 16, 31, "images/bad-flower.png");
new Downgrade("badFlower", 593, 180, 16, 31, "images/bad-flower.png");

// Flower
new Flower("goodFlower", 237, 196, 14, 30, "images/good-flower.png");
new Flower("goodFlower", 1247, 187, 14, 30, "images/good-flower.png");
new Flower("goodFlower", 781, 251, 14, 30, "images/good-flower.png");
new Flower("goodFlower", 404, 355, 14, 30, "images/good-flower.png");
new Flower("goodFlower", 1073, 249, 14, 30, "images/good-flower.png");
new Flower("goodFlower", 670, 403, 14, 30, "images/good-flower.png");
new Flower("goodFlower", 1329, 359, 14, 30, "images/good-flower.png");
new Flower("goodFlower", 1464, 136, 14, 30, "images/good-flower.png");

// Platforms
new Platform("cloud", 210, 220, 108, 46, "images/cloud.png");
new Platform("cloud", 360, 90, 108, 46, "images/cloud.png");
new Platform("cloud", 530, 215, 108, 46, "images/cloud.png");
new Platform("cloud", 1220, 210, 108, 46, "images/cloud.png");
new Platform("cloud", 1400, 170, 108, 46, "images/cloud.png");

new Platform("bush", 350, 388, 118, 48, "images/bush.png");
new Platform("bush", 930, 388, 118, 48, "images/bush.png");
new Platform("bush", 1300, 388, 118, 48, "images/bush.png");

new Platform("leaves", 747, 284, 96, 46, "images/tree-leaves.png");
new Platform("leaves", 1040, 280, 96, 46, "images/tree-leaves.png");
new Platform("leaves", 900, 210, 96, 46, "images/tree-leaves.png");

// Start
new Platform("stair", 0, 413, 80, 23, "images/stair.png");

// You won screen when colliding
new Fence("fence", 1569, 394, 31, 42, "images/fence.png")

// GameLoop is called when startbutton is clicked
// startbutton calls function called start in gameManager
