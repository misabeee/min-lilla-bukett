let aIsPressed = false;
let dIsPressed = false;
let wIsPressed = false;


function startMovement(e) {
    if (e.key === "a") {
        aIsPressed = true;
        alice.facingDirection = -1;
        alice.moveBy.left = -alice.moveVelocity;        
        alice.setCurrentAnimationByName("walk_left");   
    }
    if (e.key === "d") {
        dIsPressed = true;
        alice.facingDirection = 1;
        alice.moveBy.left = alice.moveVelocity;
        alice.setCurrentAnimationByName("walk_right");
    }
    if (e.key === "w") {
        if (alice.isFalling) return;
        wIsPressed = true;
        if (alice.facingDirection === -1) {
            alice.startJump = true;
            alice.setCurrentAnimationByName("jump_left");
        } else if (alice.facingDirection === 1) {
            alice.startJump = true;
            alice.setCurrentAnimationByName("jump_right");
        }
    }
}
addEventListener("keydown", startMovement);



function stopMovement(e) {
    if (e.key === "a") {
        aIsPressed = false;
        alice.moveBy.left = 0;
        alice.setCurrentAnimationByName("idle_left");
    }
    if (e.key === "d") {
        dIsPressed = false;
        alice.moveBy.left = 0;
        alice.setCurrentAnimationByName("idle_right");
    }
    if (e.key === "w") {
        wIsPressed = false;
        if (alice.facingDirection === -1) {
            alice.startJump = false;
            alice.setCurrentAnimationByName("idle_left");
        } else if (alice.facingDirection === 1) {
            alice.startJump = false;
            alice.setCurrentAnimationByName("idle_right");
        }
    }
}
addEventListener("keyup", stopMovement);