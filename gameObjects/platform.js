class Platform extends ImageObjectStill {

    constructor(name, x, y, width, height, src) {
        super(name, x, y, width, height, src);
        console.log("Platform has been created");
        this.isRigid = true;
    }

    onCollision(otherObject) {
        if (otherObject.name == "player" || otherObject.name == "enemy") {
            otherObject.restorePosition();
        }
    }
}