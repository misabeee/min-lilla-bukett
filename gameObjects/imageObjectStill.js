class ImageObjectStill extends GameObject {
    image;
    currentSourceX = 0;
    currentSourceY = 0;
    isLoaded = false;

    constructor(name, x, y, width, height, src) {
        super(name, x, y, width, height);
        this.image = new Image();
        this.image.src = src;
        this.image.addEventListener("load", () => {
            this.isLoaded = true;
        });    
    }
   
    draw() {
        if (this.isLoaded) {
            gameManager.canvas.drawLayer.drawImage(this.image, this.currentSourceX, this.currentSourceY, this.dimensions.width, this.dimensions.height, this.position.x, this.position.y, this.dimensions.width, this.dimensions.height);
        }
    }
}