class Upgrade extends ImageObjectStill{

    constructor(name, x, y, width, height, src) {
        super(name, x, y, width, height, src);
        console.log("Apple has been created");
    }

    // Apple disappears
    onCollision(otherObject) {
        if(otherObject.name == "player") {
            this.isActive = false;
        }
    }
}