class Fence extends Platform {

    constructor(name, x, y, width, height, src) {
        super(name, x, y, width, height, src);
        console.log("Fence has been created");
        this.isRigid = true;
    }

    // You made it!
    onCollision(otherObject) {
        if(otherObject.name == "player") {
            otherObject.restorePosition();
        }
    }
}

