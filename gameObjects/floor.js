class Floor extends ImageObjectStill {

    constructor(name, x, y, width, height, src) {
        super(name, x, y, width, height, src);
        console.log("Floor has been created");
        this.isRigid = true;
    }

    onCollision(otherObject) {
        if (otherObject.name == "player" || otherObject.name == "enemy") {
            otherObject.restorePosition();
        }
    }  
}