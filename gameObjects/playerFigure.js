class PlayerFigure extends ImageObject {
    moveBy = {
	    "left": 0,
	    "top": 0
    };

    moveVelocity = 2;
    startJump = false;
    facingDirection = 1;
    score = new Score("score");
    

    constructor(name, x, y, width, height, src) {
        super(name, x, y, width, height, src);
        console.log("Alice has been created");
        this.useGravity = true;
        this.mass = .6;
    }

    update() {

        this.position.x += this.moveBy.left;
        this.position.y += this.moveBy.top;
        this.checkWorldPostion();

        if (this.startJump) {
            this.addAntiGravityForce(200);
            this.startJump = false;
        }
        
    }
    
    checkWorldPostion() {
        if (this.boundaries.getBottomBoundary() <= gameManager.canvas.canvasBoundaries.top) {
            this.position.y = gameManager.canvas.canvasBoundaries.bottom;
        }
        else if (this.boundaries.getTopBoundary() >= gameManager.canvas.canvasBoundaries.bottom) {
            this.position.y = gameManager.canvas.canvasBoundaries.top - this.dimensions.height;
        }
        else if (this.boundaries.getRightBoundary() <= gameManager.canvas.canvasBoundaries.left) {
            this.position.x = gameManager.canvas.canvasBoundaries.right;
        }
        else if (this.boundaries.getLeftBoundary() >= gameManager.canvas.canvasBoundaries.right) {
            this.position.x = gameManager.canvas.canvasBoundaries.left - this.dimensions.width;
        }
    }

    onCollision(otherObject) {
        if(otherObject.name == "apple") {                           // Upgrade, faster movement
            this.moveVelocity = 4;                                 
            if (aIsPressed) this.moveBy.left = -this.moveVelocity;  
            if (dIsPressed) this.moveBy.left = this.moveVelocity;
        } 
        else if(otherObject.name == "badFlower") {                  // Downgrade, slower movement
            this.moveVelocity = 1;
            if (aIsPressed) this.moveBy.left = -this.moveVelocity;  
            if (dIsPressed) this.moveBy.left = this.moveVelocity;
        } 
        else if(otherObject.name == "enemy") {                      // Game Over Screen
            gameManager.stop();                             
        }
        else if(otherObject.name == "fence") {                      // Game done - you won screen
            gameManager.won();                              
        }
        else if(otherObject.name == "goodFlower") {                 // Score Counter
            this.score.increaseScore();
        }
    }
}