class Downgrade extends ImageObjectStill{

    constructor(name, x, y, width, height, src) {
        super(name, x, y, width, height, src);
        console.log("BadFlower has been created");
    }

    // Bad Flower disappears
    onCollision(otherObject) {
        if(otherObject.name == "player") {
            this.isActive = false;
            
        }
    }
}