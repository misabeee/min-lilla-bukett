class Flower extends ImageObjectStill{

    constructor(name, x, y, width, height, src) {
        super(name, x, y, width, height, src);
        console.log("Flower has been created");
    }

    // Flower disappears
    onCollision(otherObject) {
        if(otherObject.name == "player") {
            this.isActive = false;
        }
    }
}